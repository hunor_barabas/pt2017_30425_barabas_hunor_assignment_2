package com.company;

import java.util.List;

/**
 * Created by Mortimer on 4/4/2017.
 */
public interface Strategy {

    public void addTask(List<Server> serverList, Task t);
}
