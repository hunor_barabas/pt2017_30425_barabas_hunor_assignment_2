package com.company;

import javax.swing.*;

/**
 * Created by Mortimer on 4/4/2017.
 */
public class Main {


    public static void main(String[] args){

        JTextField clientNoField = new JTextField(3);
        JTextField minProcessingField = new JTextField(4);
        JTextField maxProcessingField = new JTextField(4);
        JTextField timeLimitField = new JTextField(3);
        JTextField serverNoField = new JTextField(1);
        String[] selStrings = {"shortest time", "shortest queue"};
        Selections select;

        JPanel settingsPanel = new JPanel();
        settingsPanel.add(new JLabel("Number of clients:"));
        settingsPanel.add(clientNoField);
        settingsPanel.add(Box.createVerticalStrut(15));
        settingsPanel.add(new JLabel("Min processing time:"));
        settingsPanel.add(minProcessingField);
        settingsPanel.add(Box.createVerticalStrut(15));
        settingsPanel.add(new JLabel("Max processing time:"));
        settingsPanel.add(maxProcessingField);
        settingsPanel.add(Box.createVerticalStrut(15));
        settingsPanel.add(new JLabel("Time limit:"));
        settingsPanel.add(timeLimitField);
        settingsPanel.add(Box.createVerticalStrut(15));
        settingsPanel.add(new JLabel("Servers:"));
        settingsPanel.add(serverNoField);
       JComboBox jcomb = new JComboBox(selStrings);
        settingsPanel.add(jcomb);

        int result = JOptionPane.showConfirmDialog(null, settingsPanel, "Please enter Simulation settings", JOptionPane.OK_CANCEL_OPTION);
       if(result == JOptionPane.OK_OPTION) {
           int cNo = Integer.parseInt(clientNoField.getText());
           int minP = Integer.parseInt(minProcessingField.getText());
           int maxP = Integer.parseInt(maxProcessingField.getText());
           int tLimit = Integer.parseInt(timeLimitField.getText());
           int sLimit = Integer.parseInt(serverNoField.getText());
           if(selStrings[jcomb.getSelectedIndex()] == "shortest time"){
               select =  Selections.SHORT_TIME;
           }else{
               select = Selections.SHORT_QUEUE;
           }
           Simulator simulator = new Simulator(cNo, minP, maxP, tLimit, sLimit, select);
           Thread t = new Thread(simulator);
           t.start();
       }



    }
}
