package com.company;

/**
 * Created by Mortimer on 4/4/2017.
 */
public class Task {

    private int arrivalTime;
    private int processingTime;
    private int waitingOnServer;
    private int finishTime = arrivalTime + processingTime + waitingOnServer;
    private int index;

    public Task(int arrivalTime, int processingTime, int index){
        this.index = index;
        setArrivalTime(arrivalTime);
        setProcessingTime(processingTime);
        setWaitingOnServer(waitingOnServer);
        setFinishTime(getArrivalTime()+getProcessingTime()+getWaitingOnServer());
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getWaitingOnServer() {
        return waitingOnServer;
    }

    public void setWaitingOnServer(int waitingOnServer) {
        this.waitingOnServer = waitingOnServer;
    }

    public int getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(int finishTime) {
        this.finishTime = finishTime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
