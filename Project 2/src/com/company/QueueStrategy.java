package com.company;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mortimer on 4/4/2017.
 */
public class QueueStrategy implements Strategy {

    private AtomicInteger min = new AtomicInteger(1000000);
    private Server noted;

        @Override
        public void addTask(List<Server> serverList, Task t) {
            for(Server s: serverList){
                if (min.intValue() > s.getQueueSize().intValue()) {
                    min = s.getQueueSize();
                    noted = s;
                }

            }
            synchronized (this) {
                noted.addTask(t);
            }

    }
}
