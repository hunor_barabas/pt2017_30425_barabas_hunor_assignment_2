package com.company;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mortimer on 4/4/2017.
 */
public class Server implements Runnable {
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private AtomicInteger queueSize;
    public String name;
    private PrintWriter writer;
    private Task processed;
    private AtomicInteger load;

    public Server(String name, PrintWriter writer){
        setTasks(new ArrayBlockingQueue<Task>(100));
        setWaitingPeriod(new AtomicInteger(0));
        setQueueSize(new AtomicInteger(0));
        this.name = name;
        this.writer = writer;
        this.processed = new Task(0,0, 0);
        this.load = new AtomicInteger(0);
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }


    public void addTask(Task newTask){
        try {
            tasks.put(newTask);
           writer.println(getCurrentTimeStamp()+ ": " + name + ": Started Task"+newTask.getIndex());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        getWaitingPeriod().addAndGet(newTask.getProcessingTime());
        getQueueSize().incrementAndGet();
        load.addAndGet(newTask.getProcessingTime());
    }




    @Override
    public void run() {
        while(true) {
            try {
                Task t = tasks.take();
                processed = t;
               // writer.println(this.name + ": Remaining time = " + getWaitingPeriod()+"Processing time:"+t.getProcessingTime());
                Thread.sleep(t.getProcessingTime());
                getWaitingPeriod().addAndGet(-t.getProcessingTime());
                getQueueSize().decrementAndGet();
                writer.println(getCurrentTimeStamp()+ ": " + this.name +": Finished Task"+t.getIndex());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Task getProcessed() {
        return processed;
    }

    public void setProcessed(Task processed) {
        this.processed = processed;
    }

    public BlockingQueue<Task> getTasks() {
        return tasks;
    }

    public void setTasks(BlockingQueue<Task> tasks) {
        this.tasks = tasks;
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public AtomicInteger getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(AtomicInteger queueSize) {
        this.queueSize = queueSize;
    }

    public AtomicInteger getLoad() {
        return load;
    }

    public void setLoad(AtomicInteger load) {
        this.load = load;
    }

}
