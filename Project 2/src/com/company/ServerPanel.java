package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Mortimer on 4/5/2017.
 */
public class ServerPanel extends JPanel {

    private JLabel statlabel1, statlabel2;
    private JTextField status1, status2;
    private Server server;
    private BlockingQueue<Task> tasks;
    private List<String> stats;
    private JList<String> jlist;
    private String[] statarr;
    private Task processed;
    private String[] proc;


    public ServerPanel(String name, Server server){
        Dimension size = getPreferredSize();
        size.height = 100;
        size.width = 200;
        Color c = new Color(184, 219,217 );
        this.setBackground(c);
        setPreferredSize(size);
        setBorder(BorderFactory.createTitledBorder(name));
        this.server = server;
        this.tasks = server.getTasks();
        stats = new ArrayList<String>();
        statarr = new String[40];
        setLayout(new GridLayout(2, 1));
        processed = server.getProcessed();


        for(Task t: tasks){
            stats.add("Task - processing time:" + t.getProcessingTime());
        }
        populateArr(statarr, (ArrayList<String>) stats);
        jlist = new JList<String>(statarr);
        jlist.setVisibleRowCount(40);
        jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
       // statarr[3] = "Lol";
        add(new JScrollPane(jlist));





    }


    private void populateArr(String[] arr, ArrayList<String> arrayList){
        for (int i = 0; i < arrayList.size(); i++){
            arr[i] = arrayList.get(i);
        }
    }

    public void displayData(){
        this.removeAll();
        this.processed = server.getProcessed();
        this.tasks = server.getTasks();
        stats = new ArrayList<String>();
        statarr = new String[40];
        if(server.getQueueSize().intValue() > 0) stats.add("Processing T"+processed.getIndex()+": " + processed.getProcessingTime()+ "ms");
        for(Task t: tasks){
            stats.add("T" + t.getIndex()+" :" + t.getProcessingTime()+ "ms");
        }
        this.statlabel1 = new JLabel("Tasks: " + server.getQueueSize()+"   "+"Remaining Time: " + server.getWaitingPeriod());
        //this.statlabel2 = new JLabel("Average p. time:" + server.getAvg());
        populateArr(statarr, (ArrayList<String>) stats);
        jlist = new JList<String>(statarr);
        jlist.setVisibleRowCount(40);
        jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        add(new JScrollPane(jlist));
        add(statlabel1);
        //add(statlabel2);
    }

    public JTextField getStatus1() {
        return status1;
    }

    public void setStatus1(JTextField status1) {
        this.status1 = status1;
    }

    public JTextField getStatus2() {
        return status2;
    }

    public void setStatus2(JTextField status2) {
        this.status2 = status2;
    }
}
