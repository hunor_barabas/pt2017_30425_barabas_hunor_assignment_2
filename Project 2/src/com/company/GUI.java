package com.company;

import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import java.awt.*;

/**
 * Created by Mortimer on 4/4/2017.
 */
public class GUI extends JFrame {

    private JPanel jpan;
    private int HEIGHT = 300, WIDTH = 850;
    private JLabel status1 = new JLabel("Task not completed");
    private JLabel status2 = new JLabel("Task not completed");
    private List<ServerPanel> serverPanelList;
    private Scheduler scheduler;



    public GUI(Scheduler scheduler){
        super("Queue Simulator");
        this.scheduler = scheduler;
        serverPanelList = new ArrayList<>();
        //Layout
        setLayout(new GridLayout(1,4));
        this.setSize(WIDTH,HEIGHT);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        for(int i = 0; i < scheduler.getServerLimit(); i++){
            serverPanelList.add(new ServerPanel("S"+(i+1),scheduler.getServerList().get(i)));
        }


        displayAll();
        this.setVisible(true);


    }



    public void displayAll(){
        //this.removeAll();

        for(ServerPanel p: serverPanelList){
            this.add(p);
            p.displayData();
        }
        this.repaint();
        this.revalidate();

    }
}
