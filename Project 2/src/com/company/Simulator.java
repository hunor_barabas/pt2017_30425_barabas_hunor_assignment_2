package com.company;

import javax.swing.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * Created by Mortimer on 4/4/2017.
 */
public class Simulator implements Runnable {

    public Selections sel = Selections.SHORT_TIME;
    public int timeLimit = 100;
    public int minProcessingTime = 2000;
    public int maxProcessingTime = 6000;
    public int serverNo = 3;
    public int clientNo = 100;
    private Scheduler scheduler;
    private GUI gui;
    //private SimFrame frame;
    private List<Task> clientList;
    boolean append = true;
    private int totalWaiting;
    private int avgProc;
    private int maxProc, minProc;

    public Simulator(int clientNo, int minP, int maxP, int tLimit, int serverNo, Selections sel) {
        this.timeLimit = tLimit;
        this.clientNo = clientNo;
        this.sel = sel;
        this.minProcessingTime = minP;
        this.maxProcessingTime = maxP;
        this.serverNo = serverNo;
        setScheduler(new Scheduler(serverNo, timeLimit, sel));
        this.clientList = Collections.synchronizedList(new ArrayList<>());
        gui = new GUI(getScheduler());
        maxProc = 0;
        minProc = 100000;

        for (int i = 0; i < this.clientNo; i++) {
            Task c = generateRandomClient(i+1);
            getClientList().add(c);
            totalWaiting+= c.getProcessingTime();
            if (c.getProcessingTime() > maxProc) maxProc = c.getProcessingTime();
            if (c.getProcessingTime() < minProc) minProc = c.getProcessingTime();
        }

        avgProc = totalWaiting/clientNo;
        Collections.sort(getClientList(), (o1, o2) -> o1.getArrivalTime() < o2.getArrivalTime() ? -1
                : o1.getArrivalTime() > o2.getArrivalTime() ? 1
                : 0);


    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    private Task generateRandomClient(int index){
        Random random = new Random();
        int low = this.minProcessingTime;
        int high = this.maxProcessingTime;

        int processingTime = random.nextInt(high-low) + low;
        int arrivalTime = random.nextInt(this.timeLimit);

        Task t = new Task(arrivalTime, processingTime, index);
        return t;

    }

    @Override
    public void run() {
        int currentTime = 0;
        List<Task> toTake = new ArrayList<>();
        while (currentTime < (timeLimit + 20)){
                for (Task t : clientList) {
                    if (t.getArrivalTime() == currentTime) {
                            scheduler.dispatchTask(t);
                            toTake.add(t);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                clientList.removeAll(toTake);
               // if(getClientList().get(0).getArrivalTime() == currentTime) clientList.remove(0);
            gui.displayAll();
            //update UI
            currentTime++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        scheduler.getWriter().println("--------------------------------------");
        scheduler.getWriter().println("Simulation results: \n");
        scheduler.getWriter().println("Total processing time: " + totalWaiting);
        scheduler.getWriter().println("Average processing time: " + avgProc);
        scheduler.getWriter().println("Maximum processing time: " + maxProc);
        scheduler.getWriter().println("Minimum processing time: " + minProc);
        for(int i = 0; i<serverNo; i++){
            scheduler.getWriter().println("Load on " + scheduler.getServerList().get(i).name + " = " + scheduler.getServerList().get(i).getLoad());
        }
        scheduler.getWriter().close();
        JOptionPane over = new JOptionPane("Over");
        JOptionPane.showMessageDialog(gui,"Simulation over");

    }


    public List<Task> getClientList() {
        return this.clientList;
    }

    public void setClientList(List<Task> clientList) {
        this.clientList = clientList;
    }
}
