package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Mortimer on 4/4/2017.
 */
public class Scheduler {

    private List<Server> serverList;
    private Strategy strat;
    private int serverLimit;
    private int taskLimit;
    private PrintWriter writer;
    private List<Thread> threadList;


    public Scheduler(int sLimit, int tLimit, Selections sel) {
        changeStrategy(sel);
        setServerLimit(sLimit);
        setTaskLimit(tLimit);
        serverList = Collections.synchronizedList(new ArrayList<Server>());
        threadList = Collections.synchronizedList(new ArrayList<Thread>());
        String fileName = "log.txt";
        try {
            this.writer = new PrintWriter(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        writer.println("Logging...");

        for(int i = 0; i< sLimit; i++){
            serverList.add(new Server("S"+(i+1),writer));
            threadList.add(new Thread(serverList.get(i)));
        }

       for(Thread t: threadList){
           t.start();
       }
    }

    public void changeStrategy(Selections sel){
        if(sel == Selections.SHORT_QUEUE){
            setStrat(new QueueStrategy());
        }
        if(sel == Selections.SHORT_TIME){
            setStrat(new TimeStrategy());
        }

    }

    public void dispatchTask(Task t){
        strat.addTask(getServerList(),t);
    }

    public List<Server> getServerList() {
        return serverList;
    }

    public void setServerList(List<Server> serverList) {
        this.serverList = serverList;
    }

    public Strategy getStrat() {
        return strat;
    }

    public void setStrat(Strategy strat) {
        this.strat = strat;
    }

    public int getServerLimit() {
        return serverLimit;
    }

    public void setServerLimit(int serverLimit) {
        this.serverLimit = serverLimit;
    }

    public int getTaskLimit() {
        return taskLimit;
    }

    public void setTaskLimit(int taskLimit) {
        this.taskLimit = taskLimit;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }
}
